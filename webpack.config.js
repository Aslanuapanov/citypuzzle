const path = require('path')
const {resolve} = require('path')
const webpack = require('webpack')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')

const isProd = process.env.NODE_ENV === 'production'
const isDev = !isProd
const jsLoaders = () => {
  const loaders = [
    {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env']
      }
    }
  ]

  return loaders
}

const filename = ext => isDev ? `bundle.${ext}` : `bundle.[hash].${ext}`

module.exports = {
  context: path.resolve(__dirname, 'src'),
  mode: 'development',
  entry: ['@babel/polyfill', './js/index.js', './js/faq.js', './js/game.js'],
  output: {
    filename: filename('js'),
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ['.js'],
    alias: {
      '@': path.resolve(__dirname, 'src'),
    }
  },
  devServer: {
    contentBase : ['index.html'], 
    watchContentBase : true,
    port: 3000,
    hot: isDev,
  },
  devtool: isDev ? 'source-map' : '',
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default']
    }),
    new HTMLWebpackPlugin({
      filename: 'index.html',
      template: '../src/index.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      filename: 'activity.html',
      template: '../src/activity.html.ejs',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      filename: 'faq.html',
      template: '../src/faq.html.ejs',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      filename: 'game.html',
      template: '../src/game.html.ejs',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'src/favicon.ico'),
          to: path.resolve(__dirname, 'dist')
        },
        { from: '../src/img', to: 'img'},
        { from: '../src/index.html', to: './index.html' },
        { from: '../src/activity.html.ejs', to: './activity.html' },
        { from: '../src/faq.html.ejs', to: './faq.html' },
        { from: '../src/game.html.ejs', to: './game.html' },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: filename('css')
    }),
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: isDev,
              reloadAll: true
            }
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: jsLoaders(),
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
              esModule: false,
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              name: 'img/[name].[ext]',
              limit: false,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'img/[name].[ext]',
              limit: false,
              esModule: false,
            }
          },
        ],
      },
      {
        test:/\.html$/,
        use: [
          'html-loader?minimize=false'
        ],
      },
    ]
  },
}